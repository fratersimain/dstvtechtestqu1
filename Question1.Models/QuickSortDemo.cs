﻿using Question1.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question1.Models
{
    public class QuickSortDemo
    {
        public static string Run(string input)
        {
            ///Dependencies (normally set with IOC)
            StringUtilities stringUtilities = new StringUtilities();

            ///Convert to lowercase
            input = input.ToLower();

            ///Convert to char array
            char[] charArray = stringUtilities.ConvertToCharArray(input);

            ///Remove punctuation and whitespace
            charArray = stringUtilities.RemovePunctuationAndWhitespace(charArray);

            ///Do a manual quick sort on array
            string result = stringUtilities.QuickSort(charArray, 0, charArray.Length - 1);

            return result;
        }
    }
}
