﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question1.Models;
using System.Diagnostics;

namespace Question1.Tests
{
    [TestClass]
    public class QuickSortDemoTests
    {
        [TestMethod]
        public void QuickSortDemoTest()
        {
            ///Arrange
            string input = "Contrary to popular belief, the pink unicorn flies east";
            string correctResult = "aaabcceeeeeffhiiiiklllnnnnooooppprrrrssttttuuy";

            ///Act
            string result = QuickSortDemo.Run(input);

            ///Assert
            Assert.AreEqual(result,correctResult);   
        }

       


    }
}
