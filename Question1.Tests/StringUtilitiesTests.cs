﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Question1.Utilities;
using System.Linq;

namespace Question1.Tests
{
    [TestClass]
    public class StringUtilitiesTests
    {

        static StringUtilities _stringUtilities;

        public StringUtilitiesTests()
        {
            ///Define dependencies (would use normally use IOC)
            _stringUtilities = new StringUtilities();
        }

        [TestMethod]
        public void QuickSortTest()
        {
            ///Arrange
            string input = "akgpba";
            char[] inputCharArray = input.ToCharArray();

            string correctResult = "aabgkp";

            ///Act
            string result = _stringUtilities.QuickSort(inputCharArray, 0, inputCharArray.Length - 1);

            ///Assert
            Assert.AreEqual(result, correctResult);
        }

        [TestMethod]
        public void RemovePunctuationAndWhitespaceTest()
        {
            ///Arrange
            string input = "az,.'bg kd ";
            char[] inputCharArray = input.ToCharArray();
            char[] correctResult = new char[] { 'a', 'z', 'b', 'g', 'k', 'd' };

            ///Act
            char[] result = _stringUtilities.RemovePunctuationAndWhitespace(inputCharArray);

            ///Assert
            Assert.IsTrue(Enumerable.SequenceEqual(result, correctResult));
        }

        [TestMethod]
        public void ConvertToCharArrayTest()
        {
            ///Arrange
            string input = "brfghy";


            char[] correctResult = new char[] { 'b', 'r', 'f', 'g', 'h', 'y' };

            ///Act
            char[] result = _stringUtilities.ConvertToCharArray(input);

            ///Assert
            Assert.IsTrue(Enumerable.SequenceEqual(result, correctResult));
        }
    }
}
