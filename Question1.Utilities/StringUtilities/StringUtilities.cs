﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question1.Utilities
{
    public class StringUtilities
    {  
        
        private void QuickSortSwap(char[] array, int left, int right)
        {
            char Temp = array[right];
            array[right] = array[left];
            array[left] = Temp;
        }


        public char[] ConvertToCharArray(string input)
        {
            char[] charArray = input.ToCharArray();
            return charArray;
        }

        public char[] RemovePunctuationAndWhitespace(char[] charArray)
        {
            charArray = charArray.Where(c => !char.IsPunctuation(c) && !Char.IsWhiteSpace(c)).ToArray();

            return charArray;
        }

      

        public string QuickSort(char[] array, int left, int right)
        {
            int LHold = left;
            int RHold = right;
            Random ObjRan = new Random();
            int Pivot = ObjRan.Next(left, right);
            QuickSortSwap(array, Pivot, left);
            Pivot = left;
            left++;

            while (right >= left)
            {
                int cmpLeftVal = array[left].CompareTo(array[Pivot]);
                int cmpRightVal = array[right].CompareTo(array[Pivot]);

                if ((cmpLeftVal >= 0) && (cmpRightVal < 0))
                {
                    QuickSortSwap(array, left, right);
                }
                else
                {
                    if (cmpLeftVal >= 0)
                    {
                        right--;
                    }
                    else
                    {
                        if (cmpRightVal < 0)
                        {
                            left++;
                        }
                        else
                        {
                            right--;
                            left++;
                        }
                    }
                }
            }
            QuickSortSwap(array, Pivot, right);
            Pivot = right;
            if (Pivot > LHold)
            {
                QuickSort(array, LHold, Pivot);
            }
            if (RHold > Pivot + 1)
            {
                QuickSort(array, Pivot + 1, RHold);
            }

            ///Prepare result
            string result = new string(array);

            return result;
        }

    }
}
