﻿using Question1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question1.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Get user input
            bool runDemo = true;
            do
            {
                ///Display instructions 
               System.Console.WriteLine("Enter a line of text or x to exit");
               System.Console.WriteLine();
                string input;

                ///Get user input
                input = System.Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    System.Console.WriteLine();
                    continue;
                }

                ///in input is escape 
                if (input.ToLower() == "x")
                {
                    System.Console.WriteLine("bye");

                    ///trip flag
                    runDemo = false;

                    //Quit main
                    continue;
                }

                ///Perform sort
                string sortedInput = QuickSortDemo.Run(input);

                ///Show output
                System.Console.WriteLine(sortedInput);

            } while (runDemo == true);

            ///The end
            return;
        }
    }
}
